package net.qqxh.controller.apiUtil;

/**
 * @author jason
 */
public class BaseJson {
    /**
     *  请求状态
     */
    private String status;
    /**
     * 数据错误/成功等标题
     */
    private String msg;
    /**
     * 用于描述详细信息：比如：手机号不能为空 -没有信息时不会返回此字段
     */
    private String detail;
    /**
     * 业务数据
     */
    private Object Data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Object getData() {
        return Data;
    }

    public void setData(Object data) {
        Data = data;
    }
}
